;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns tsp-test.core
  (:require [clojure.test :refer :all]
            [tsp.pathfinding :refer :all]
            [tsp.rating :refer :all])
  (:use [tsp.utilities]))


(defn close? [x y tolerance]
  (< (Math/abs (- x y)) tolerance))


(def node1 (->TSPNode 1 100 100))
(def node2 (->TSPNode 2 200 200))
(def node3 (->TSPNode 3 200 300))
(def node4 (->TSPNode 4 1000 1000))
(def node5 (->TSPNode 5 100 200))
(def node6 (->TSPNode 6 300 100))
(def node7 (->TSPNode 7 300 300))
(def node8 (->TSPNode 8 300 200))

(def testnodes [node1 node2 node3 node4])

(def testnodes2
  [(->TSPNode 1 1000 300)
   (->TSPNode 2 1000 600)
   (->TSPNode 3 1000 900)
   (->TSPNode 4 1000 1200)
   (->TSPNode 5 1000 1500)
   (->TSPNode 6 1000 1800)
   (->TSPNode 7 1000 2100)
   (->TSPNode 8  600 1800)
   (->TSPNode 9  200 1200)
   (->TSPNode 10 500 500)])

(def testnodes3 [node1 node2 node3 node4 node5 node6 node7 node8])

(deftest conversiontest
  (is (= (match->node '(["1 100 100" "1" "100" "100"]))
         node1)))

(deftest distancetest
  (is (close? (euclidean-distance node1 node2)
              141.42
              0.1)))

(deftest neighbourdistandestest
  (is (= (map round2 (neighbour-distances (first testnodes) testnodes))
         '(0.0 141.42 223.61 1272.79))))

(deftest chtest
  (is (= (convex-hull testnodes) [node1 node3 node4]))
  (is (= (convex-hull testnodes3) [node1 node5 node3 node4 node6])))

(deftest accumdistancetest
  (is (close? (accumulated-distance (first testnodes2) testnodes2)
              9595.0934
              0.1)))

(deftest maxdistancemodel
  (is (= (max-distance-model testnodes) (list node4))))


(deftest nearest-neighbour-test
  (is (= (nearest-neighbour (first testnodes) (rest testnodes)))
      node2))

(deftest nearest-k-neighbours-test
  (is (= (nearest-k-neighbours (first testnodes)
                               (rest testnodes)
                               2)
         [node2 node3])))


(deftest orientation-test
  (is (= (orientation node1 node2 node3)
         2))
  (is (= (orientation node1 node2 (->TSPNode 3 300 300))
         0))
  (is (= (orientation node2 node1 node3)
         (orientation node3 node2 node1)
         1)))


(deftest in-bounds?-test
  (is (in-bounds? node1 node2 (->TSPNode 3 300 300)))
  (is (in-bounds? node1 node2 node3))
  (is (not (in-bounds? node1 node3 node2))))


(deftest intersection-test
  (is (not (intersect? node1 node2 node2 node3)))
  (is (not (intersect? node2 node3 node1 node2)))
  (is (not (intersect? node3 node1 node1 node2)))
  (is (intersect? node2 node5 node1 node3))
  (is (intersect? node1 node4 node3 node2))
  (is (intersect? node4 node5 node1 node3)))


(deftest pairwise-intersections-test
  (is (not (pairwise-intersections? node1 node2 [node2 node4 node3 node5 node1])))
  (is (pairwise-intersections? node1 node3 [node3 node4 node2 node5 node1])))


(deftest has-intersections-test
  (is (not (has-intersections? [node1 node2 node4 node3 node5 node1])))
  (is (has-intersections? [node1 node2 node5 node3 node4 node1]))
  (is (not(has-intersections? [node1 node2])))
  (is (not (has-intersections? [node1 node2 node5]))))


(deftest in?-test
  (is (in? [1 2 3 4] 4))
  (is (not (in? [1 2 3 4] nil)))
  (is (in? [node2 node3 node1 node4] node1)))

(deftest drop-until-test
  (is (= (drop-until 4 [1 2 3 4 5 6])
         [4 5 6])))


(deftest intact-hull-test
  (is (intact-hull? [node1 node2  node5 node7 node3] testnodes3))
  (is (intact-hull? [node3 node7 node5 node2 node1] testnodes3))
  (is (intact-hull? [node7 node2 node8] testnodes3))
  (is (not (intact-hull? [node3 node2 node1 node5] testnodes3))))

(deftest straight-line-test
  (is (= (successor-straightness node1 node2 [node3 node4 node5 node6])
         '(45.0 0.0 135.0 90.0))))
(deftest ma-straightness-test
  (is (close? (straightness node1 node2 node6 [node3 node4 node5])
              0.333
              0.01)))



(deftest splitting-points?-test
  (is (= 1 (splitting-points? node1 node2 [node3 node4 node5])))
  (is (< 1 (splitting-points? node1 node2 [node3 node4 node5 node6]))))

(deftest bool2int-test
  (is (= (bool2int true) 1))
  (is (= (bool2int false) 0)))


(deftest comtest
  (is (= (center-of-mass [node1 node2 node3]) '[500/3,200])))

(deftest regions-test
  (is (= (m-regions testnodes3) [[node8 node6]
                                 [node7 node3]
                                 [node2 node5]
                                 [node1]
                                 [node4]])))


(deftest com-regions-test
  (is (= (map tsp.utilities/center-of-mass (m-regions testnodes3))
         [(tsp.utilities/center-of-mass [node8 node6])
          (tsp.utilities/center-of-mass [node7 node3])
          (tsp.utilities/center-of-mass [node2 node5])
          (tsp.utilities/center-of-mass [node1])
          (tsp.utilities/center-of-mass [node4])])))


(deftest rotatetest
  (is (= (rotate-to-zero [1 2 3 0 4 5 6 1]) [0 4 5 6 1 2 3 0]))
  (is (= (rotate-to-zero [0 1 2 3 4 5 6 0]) [0 1 2 3 4 5 6 0])))
