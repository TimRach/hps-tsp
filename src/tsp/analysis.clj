; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.analysis)


(defn rate-file
  [file]
  (with-open [rdr (reader file)] 
    (map #(map read-string %)
         (map #(clojure.string/split % #" ")
              (doall (line-seq rdr))))))

(defn rate-dir
  [dirpath]
  (let [files (filter #(re-matches #".*\.sol" (.getName %))
                      (file-seq (file dirpath)))]
    (doseq [file files]
      (println (clojure.string/replace file #"\.sol$" ".json"))
      (rate-file file))))
